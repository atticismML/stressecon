import random

choiceR = ''
choiceL = ''

choiceMade = ''
points = 134
newPoints = 0

dateStr = 'none'


def randomize():
    w = random.randint(0, 1)
    x = random.randint(0, 1)
    y = random.randint(0, 1)
    z = random.randint(0, 1)
    return (w, x, y, z)

magnitude = 0
probability = 0


def getProbMag(trial):

    probability = trial[0]
    magnitude = trial[1]
    expectedUtil = magnitude*probability

    return probability, magnitude, expectedUtil
