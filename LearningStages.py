import StageLoops
import StatsSetup
import Setup
import DynamicVars
import PokeArrangement
import PokeFunctions
import Tonefunctions
import Reward
import Production
import random
from psychopy import sound, core

# responseClock = core.MonotonicClock()

#         while choiceMade == '':
#             if user.getPressed() != [0, 0, 0]:
#                 mpos = user.getPos()
#                 for poke in range(0, len(pokes)):
#                     if pokes[poke].contains(mpos):
#                         if pokes[poke].status == 'success':
#                             print "You chose the lottery"
#                             choiceMade = 'success'
#                         user.clickReset()

#         timeToRespond = responseClock.getTime()

#         # STAGE 2

#         resetPokes()
#         choiceDraw(rBetRow, rSureRow, rSide)

#         responseClock = core.MonotonicClock()

#         while choiceMade == '':

#             if user.getPressed() != [0, 0, 0]:
#                 mpos = user.getPos()
#                 for poke in range(0, len(pokes)):
#                     if pokes[poke].contains(mpos):
#                         if pokes[poke].status == 'lotteryPoke':
#                             print "You chose the lottery"
#                             choiceMade = 'lottery'
#                             newPoints = expectedUtil
#                         if pokes[poke].status == 'surePoke':
#                             print "You chose the sure bet"
#                             choiceMade = 'sure'
#                             newPoints = sureBet
#                         user.clickReset()

#         timeToRespond = responseClock.getTime()

#         # STAGE 3

#         responseClock = core.MonotonicClock()

#         while choiceMade == '':

#             if user.getPressed() != [0, 0, 0]:
#                 mpos = user.getPos()
#                 for poke in range(0, len(pokes)):
#                     if pokes[poke].contains(mpos):
#                         if pokes[poke].status == 'lotteryPoke':
#                             print "You chose the lottery"
#                             choiceMade = 'lottery'
#                             newPoints = expectedUtil
#                         if pokes[poke].status == 'surePoke':
#                             print "You chose the sure bet"
#                             choiceMade = 'sure'
#                             newPoints = sureBet
#                         user.clickReset()

#         timeToRespond = responseClock.getTime()


def interLockout(time):

    while time > 0:
        PokeFunctions.drawPokes()
        Reward.drawDispenser()
        Production.statusBar('Please wait... (' + str(time) + ')')
        Setup.mywin.update()
        core.wait(1)
        time -= 1


def pokeLearn():  # Simple Poke Learning Stage

    consecCorr = 0
    while consecCorr < 5:
        rIRow = random.randint(0, 1)
        PokeArrangement.IRowDraw(rIRow)
        Production.statusBar(
            'LS1\nPress the active poke. | Streak: ' + str(consecCorr) + '/5')
        Setup.mywin.update()

        StageLoops.singlePokeLoop()

        interLockout(4)
        consecCorr += 1
        Setup.mywin.update()
        print consecCorr


def magLearn():
 # Magnitude Discrimination Learning Stage

    consecCorr = 0
    while consecCorr < 5:
        for i in range(0, len(StatsSetup.shuffledBlocks)):

            trialA = StatsSetup.shuffledBlocks[i]
            trialB = StatsSetup.shuffledBlocks[i+1]

            if trialA[1] == trialB[1]:
                trialB[1] += 5

            if trialA[1] > trialB[1]:
                correct = trialA[1]
            else:
                correct = trialB[1]

            rBetRow, rSureRow, rSide, x = DynamicVars.randomize()

            toneOut = sound.SoundPyo(
                value=Tonefunctions.createToneValue(trialA[1], 1, rSide, trialB[1], 1))
            toneOut.play()

            PokeFunctions.resetPokes()
            PokeArrangement.choiceDraw(
                rBetRow, rSureRow, rSide, 'choiceA', 'choiceB', trialA[1], trialB[1])
            Production.statusBar(
                "LS2\nChoose the higher expected value poke. | Streak: " + str(consecCorr) + "/5")
            Setup.mywin.update()

            StageLoops.choiceLoop()
            print 'Value of what you chose: ' + str(DynamicVars.newPoints)
            print 'Value of what you should have chose: ' + str(correct)

            interLockout(4)
            if DynamicVars.newPoints == correct:
                consecCorr += 1
            else:
                consecCorr = 0

            print 'consecCorr: ' + str(consecCorr)
            i += 2


def probLearn():

    # Probability Discrimination Learning Stage

    consecCorr = 0
    while consecCorr < 5:

        for n in range(0, len(StatsSetup.shuffledBlocks)):

            trialA = StatsSetup.shuffledBlocks[n]
            trialB = StatsSetup.shuffledBlocks[n+1]
            if trialA[0] > trialB[0]:
                correct = trialA[0]
            else:
                correct = trialB[0]

            rBetRow, rSureRow, rSide, x = DynamicVars.randomize()

            toneOut = sound.SoundPyo(
                value=Tonefunctions.createToneValue(Setup.sureBet, trialA[0], rSide, Setup.sureBet, trialB[0]))
            toneOut.play()

            PokeArrangement.choiceDraw(
                rBetRow, rSureRow, rSide, 'choiceA', 'choiceB', trialA[0], trialB[0])
            Production.statusBar(
                'LS3\nChoose the higher probability choice. | Streak: ' + str(consecCorr) + '/5')
            Setup.mywin.update()
            print trialA[0]
            print trialB[0]
            choiceValue = StageLoops.choiceLoop()

            interLockout(4)
            if choiceValue == correct:
                consecCorr += 1

            n += 2


def tutorial():

    while Setup.learningMode:

        # pokeLearn()
        magLearn()
        probLearn()
        
        Setup.learningMode = False
