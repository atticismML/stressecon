# MAINTASK OF STRESS/ECON

#
#
#
# HOW TO TEST
#
# ITI requires you to press a poke. Continue to choice round.
#
# Learning in choice round:
#
# SureBet will be a steady tone, randomly in one ear or another.
# Lottery will come to other channel, with:
#     magnitude proportional to freq
#     probability proportional to a latter modulation
#
# e.g. If subject hears a very high lottery tone with very high modulation,
#       subject probably wants to pick that over the sure bet.
#
# Results stored in Results/....date
#
#
#


# import required libraries and edit preferences in-file

from Poke import *
from PokeFunctions import *
from Tonefunctions import *
from PokeArrangement import *
from StatsSetup import *
from Setup import *
from Points import *
from DataOutput import *
from DynamicVars import *
from StageLoops import *
from LearningStages import *
from Reward import *
from Production import *

# Por favor
# from os.path import expanduser, sep
# sys.path.append(expanduser("~")) + sep + "modules"
# from helpers import DBUtilsClass as db

# cnx = db.Connection()


##########################################################################
#------------------------   MAIN   ---------------------------------------
##########################################################################

# dataInit()

for trial in shuffledBlocks: # Find the shuffled blocks and trial settings in StatsSetup.py
    
    tutorial()

    rIRow, rSide, rSureRow, rBetRow = randomize() # Randomize poke placements

#-----------------------(1) INTERTRIAL LOCKOUT
# Draw the screen but have no interactivity

    drawPokes() 
    drawDispenser()
    statusBar('Please wait...')
    mywin.update()
    # core.wait(4)

#-----------------------(2) INTERTRIAL POKE TEST
# Draw the attention poke and everything else. Move on after subject has pressed the poke. 

    IRowDraw(rIRow)
    drawDispenser()
    statusBar('Click the active poke to\nmove to the choice trial.')
    mywin.update()
    singlePokeLoop()
    # Wait until press...

#----------------------(2) TRIAL: USER DECISION BETWEEN LOTTERY AND SURE BET
# Draw the choice pokes and inform user of conditions through stimulus. Move on after subject has chosen a poke. 

    probability, magnitude, expectedUtil = getProbMag(trial) # Find this function in DynamicVars.py: returns per conditions per trial

    toneStim(trial, rSide) # The audio stimulus
    choiceDraw(rBetRow, rSureRow, rSide) 
    drawDispenser()
    statusBar('Choice Trial')
    mywin.update()

    choiceLoop()
    # Wait until press...

    reward() 
    # result = [choiceMade, sureBet, expectedUtil, magnitude, probability, rSide, rBetRow, dateStr]
    # results.append(result)
    # writeData()
