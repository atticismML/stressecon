from psychopy import visual
import Setup
import DynamicVars


pileOrigin = [-13, -10]
coinscale = 2
base = 7


def coin():
    return visual.Polygon(Setup.mywin, edges=32, radius=0.5, size=(coinscale, coinscale/2))


def stackCoins(denomination, stack_xpos, ypos, fill, pileOrig=pileOrigin):
    for x in range(0, int(denomination)):
        c = coin()
        cPos = [0, 0]
        cPos[0] = stack_xpos
        cPos[1] = ypos
        ypos += coinscale*0.1
        c.setPos(cPos)
        c.fillColor = fill
        c.draw()


def stackAll(amount=DynamicVars.points, pileOrig=pileOrigin):
    h_xpos = pileOrig[0] - coinscale
    t_xpos = h_xpos + coinscale
    o_xpos = t_xpos + coinscale
    ypos = pileOrig[1]
    hundreds, tens, ones = coinUpdate(amount)
    stackCoins(hundreds, h_xpos, ypos, '#EEDD00', pileOrig)
    stackCoins(tens, t_xpos, ypos, '#CCCCCC', pileOrig)
    stackCoins(ones, o_xpos, ypos, '#EE9900', pileOrig)


def coinUpdate(amount=DynamicVars.points):
    hundreds = (amount - (amount % base**2)) / base**2
    ones = amount % base
    tens = (amount - (hundreds*base**2) - ones) / base
    return (hundreds, tens, ones)


def totalUpdate():
    stackAll(DynamicVars.points)
