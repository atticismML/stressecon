# Poke Object Class (POC)
from psychopy import visual
import Setup


class Poke(visual.Polygon):

    """A class for the pokes, which inherits from visual.Polygon"""
    status = 'inactive'  # By  default, clicking on the poke means nothing,
    # but later on they may be lotteryChoice pokes, sureBet pokes, or ITI pokes

    def __init__(self, posx, posy, inColor='#000000', outColor='#000000', scale=2.5, value=0):
            '''Initialize with position args, and optional
            color args (for inset shape and outset bevel)'''

            # Each poke carries an outer bevel
            self.outer = visual.Polygon(
                win=Setup.mywin, edges=10, radius=scale, pos=(posx, posy))
            self.outer.fillColor = outColor

            # But the poke itself shall be the inner shape:
            super(self.__class__, self).__init__(
                win=Setup.mywin, edges=10, radius=scale*0.7, pos=(posx, posy))
            self.fillColor = inColor

            # Set coordinates (centered)
            self.posx = posx
            self.posy = posy
            self.value = value

    def draw(self):
            '''Draws shape and bevel. Be sure to update window after all drawing.'''
            self.outer.draw()
            super(self.__class__, self).draw()

    def getCoord(self):
            '''Returns the poke's x and y coordinates as a tuple'''
            x = self.posx
            y = self.posy
            return (x, y)
