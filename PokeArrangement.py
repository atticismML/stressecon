import PokeFunctions
import Setup
import StatsSetup
import DynamicVars


def IRowDraw(rIRow):
    if rIRow == 1:
        # Top poke is the live one
        PokeFunctions.makeLivePoke(
            StatsSetup.pokes, Setup.black, 0, Setup.topRow, 'success')
    elif rIRow == 0:
        # Bottom poke is the live one
        PokeFunctions.makeLivePoke(
            StatsSetup.pokes, Setup.black, 0, Setup.bottomRow, 'success')

    PokeFunctions.drawPokes()


def placePoke(scale, coeff, shift=0):
    """Know where to draw the pokes on an axis Setup.
    this is based on assigned y-wise or x-wise scales, and a shift constant if needed"""

    return scale-(scale*coeff)+shift


def choiceDraw(rBetRow, rSureRow, rSide, choiceA='lotteryPoke', choiceB='surePoke', valueA=0, valueB=0):

    if rSide == 1:  # Lottery right, sure left
        DynamicVars.choiceR = choiceA
        DynamicVars.choiceL = choiceB
        sureCol = Setup.leftCol
        lotCol = Setup.rightCol
    elif rSide == 0:  # Lottery left, sure right
        DynamicVars.choiceR = choiceB
        DynamicVars.choiceL = choiceA
        lotCol = Setup.leftCol
        sureCol = Setup.rightCol

    if rSureRow == 1:
        PokeFunctions.makeLivePoke(
            StatsSetup.pokes, Setup.bright, sureCol, Setup.topRow, choiceB, valueB)
    elif rSureRow == 0:
        PokeFunctions.makeLivePoke(
            StatsSetup.pokes, Setup.bright, sureCol, Setup.bottomRow, choiceB, valueB)

    if rBetRow == 1:
        PokeFunctions.makeLivePoke(
            StatsSetup.pokes, Setup.dark, lotCol, Setup.topRow, choiceA, valueA)
    elif rBetRow == 0:
        PokeFunctions.makeLivePoke(
            StatsSetup.pokes, Setup.dark, lotCol, Setup.bottomRow, choiceA, valueA)

    PokeFunctions.drawPokes()
