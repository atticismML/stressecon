import Setup
import StatsSetup
import Points
from psychopy import event

def drawPokes():
    '''Draw all the pokes in the 'pokes[]' array'''
    for p in StatsSetup.pokes:
        p.draw()
    Points.totalUpdate()
    curr_key = event.getKeys()
    print curr_key


def coordCheck(poke, x, y):
    '''Given a specific poke, check if it has these x and y coordinates'''
    if poke.getCoord()[0] == x and poke.getCoord()[1] == y:
        return True
    else:
        return False


def makeLivePoke(pokes, color, x, y, status, value=0):
    '''Set a specific poke to have a live status and a specific color'''
    for p in StatsSetup.pokes:
        if coordCheck(p, x, y):
            p.fillColor = color
            p.status = status
            p.value = value


def makeChoicePokes(pokes, colorA, colorB, yA, yB):
    '''Set the pokes the user can click to make a decision
    mainly dynamic based on the random y value generated at runtime'''
    Setup.makeLivePoke(Setup.pokes, colorA, Setup.rightCol, yA, Setup.choiceR)
    Setup.makeLivePoke(Setup.pokes, colorB, Setup.leftCol, yB, Setup.choiceL)


def resetPokes():
    '''Set all pokes inactive again'''
    for p in StatsSetup.pokes:
        p.fillColor = Setup.mid
        p.status = 'inactive'
