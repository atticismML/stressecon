from psychopy import visual as v
import Setup

def statusBar(text,pos=(0,-6)):
	'''Displays text'''
	status = v.TextStim(Setup.mywin, text, pos=pos, height=0.6)
	status.draw()