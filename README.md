Welcome to an economic preference task in risk-taking. 
______

MainTask.py - Run

Setup.py - General Settings, Modes

StatsSetup.py - Experiment Settings

Press any key to exit. 

_____


ISSUES: Tonefunctions.py - Adjust arrays to completely isolate LR stimuli. 

TO-DO: Reinstate data output (whether via local csv or http request).
____

FUTURE: 

Time-to-respond - Implement clock for ramp up, reaction time.

Mouse tracking - Append mouse position to list during stage loops.