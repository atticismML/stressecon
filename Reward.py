import Setup
import DynamicVars
import Points
import PokeFunctions
import StageLoops
import Production
from psychopy import sound, core, visual
from psychopy import prefs
prefs.general['audioLib'] = ['pyo']

boxOrig = [0, -6]
queueOrig = [0, -7]
boxScale = 1.2

border = visual.Rect(
    Setup.mywin, width=boxScale*9, height=boxScale*4, fillColor=Setup.mid, pos=boxOrig)
pane = visual.Rect(
    Setup.mywin, width=boxScale*8, height=boxScale*3, fillColor=Setup.black, pos=boxOrig)

rewardSound = sound.SoundPyo(value="RewardSignal.wav")


def reward():

    queueReward()

    while DynamicVars.newPoints > 1:

        core.wait(0.03)
        DynamicVars.newPoints -= 1
        DynamicVars.points += 1
        rewardSound.play()

        drawActiveDispenser()
        Points.stackAll(DynamicVars.newPoints, queueOrig)

        PokeFunctions.resetPokes()
        PokeFunctions.drawPokes()

        Setup.mywin.update()

    border.fillColor = Setup.mid
    pane.fillColor = Setup.black


def queueReward():

    drawActiveDispenser()
    Points.stackAll(DynamicVars.newPoints, queueOrig)

    PokeFunctions.resetPokes()
    PokeFunctions.drawPokes()
    # Production.statusBar('Click dispenser to claim reward.')
    Setup.mywin.update()

    claimed = False
    while not claimed:
        if StageLoops.user.getPressed() != [0, 0, 0]:
            mpos = StageLoops.user.getPos()
            if border.contains(mpos) or pane.contains(mpos):
                claimed = True


def drawDispenser():
    border.draw()
    pane.draw()


def drawActiveDispenser():
    border.fillColor = Setup.mid
    pane.fillColor = Setup.bright
    border.draw()
    pane.draw()
