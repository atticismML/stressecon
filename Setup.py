from psychopy import visual

learningMode = True

#PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.ADJUSTABLE VARIABLES
sureBet = 5
magScale = 200  # Reward magnitude scaler (inverse)


# SHADES OF COLOR

mid = "#777777"
dark = "#333333"
bright = "#AAAAAA"
black = "#000000"

# SCALE OF POKES 
scale = 5
yscale = scale
xscale = scale
rightCol = scale # coord
leftCol = -scale  # coord

# DIMENSIONS
rows = 2
columns = 3
topRow = scale
bottomRow = 0  # coord
blockNum = 4

#PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.

# create universe

mywin = visual.Window([900, 600], monitor="testMonitor", units="deg", fullscr = True)

