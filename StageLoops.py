import StatsSetup
import PokeFunctions
import DynamicVars
import Tonefunctions
import Setup
import time
from psychopy import event, prefs
prefs.general['audioLib'] = ['pyo']

user = event.Mouse(True, None, None)


def singlePokeLoop():

    choiceMade = 'notPassed'
    mpos = None

    while choiceMade == 'notPassed':
        curr_key = event.getKeys()
        if curr_key != []:
            Setup.mywin.close()
        if user.getPressed() != [0, 0, 0]:  # If there's a click
            mpos = user.getPos()  # Get mouse position
            for poke in range(0, len(StatsSetup.pokes)):
                # Check if the mouse position is that of any of the pokes
                if StatsSetup.pokes[poke].contains(mpos):
                    user.clickReset()
                    # Is it the attention poke?
                    if StatsSetup.pokes[poke].status == 'success':
                        choiceMade = 'passed'

    PokeFunctions.resetPokes()


def choiceLoop():

    mpos = None
    DynamicVars.choiceMade = ''

    while DynamicVars.choiceMade == '':
        curr_key = event.getKeys()
        if curr_key != []:
            Setup.mywin.close()
        if user.getPressed() != [0, 0, 0]:  # If there's a click
            mpos = user.getPos()  # Get mouse position
            for poke in range(0, len(StatsSetup.pokes)):
                # Check if the mosue position is that of any of the pokes
                if StatsSetup.pokes[poke].contains(mpos):
                    user.clickReset()
                    # Is it the lottery choice?
                    if StatsSetup.pokes[poke].status == 'lotteryPoke':
                        print "You chose the lottery"
                        DynamicVars.choiceMade = 'lottery'
                        # New points for the reward animation: expected util
                        DynamicVars.newPoints = DynamicVars.expectedUtil
                        print DynamicVars.newPoints
                    # Is it the sure choice?
                    if StatsSetup.pokes[poke].status == 'surePoke':
                        print "You chose the sure bet"
                        DynamicVars.choiceMade = 'sure'
                        # New points for the reward animation: raw value
                        DynamicVars.newPoints = Setup.sureBet
                    # For learning stages, 'choiceA' and 'choiceB'
                    elif StatsSetup.pokes[poke].status[:6] == 'choice':
                        DynamicVars.choiceMade = str(
                            StatsSetup.pokes[poke].status)
                        DynamicVars.newPoints = StatsSetup.pokes[poke].value

    DynamicVars.dateStr = time.strftime("%c")
    PokeFunctions.resetPokes()

    return DynamicVars.newPoints


def toneStim(trial, rside):
    DynamicVars.probability, DynamicVars.magnitude, DynamicVars.expectedUtil = DynamicVars.getProbMag(
        trial)
    signal = Tonefunctions.soundMP(
        DynamicVars.magnitude, DynamicVars.probability, rside)
    signal.play()
