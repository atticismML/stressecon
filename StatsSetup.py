import Setup
import PokeArrangement
import random
from Poke import Poke

# Set the pokes up

# Get array ready PokeFunctions. We're going to make six Pokes and store them in a list
pokes = []
pokeCoords = []  # Likewise, we need six coordinates
for i in range(Setup.rows):  # n physical rows times
    for j in range(Setup.columns):  # n physical columns
        row = PokeArrangement.placePoke(Setup.yscale, i)  # Spacings based on scale
        col = PokeArrangement.placePoke(Setup.xscale, j)
        # Stores these coords in a list (for reference)
        pokeCoords.append([col, row])
        # Generate a Poke object and store these in the list
        pokes.append(Poke(posx=col, posy=row, inColor=Setup.mid))


#PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.PokeFunctions.

# Statistic setup

# Set the stats (risk combos)

probs = [0.25, 0.5, 0.75]
mags = [5, 6, 7, 8, 10, 12, 14, 16, 19, 23,
        27, 31, 37, 44, 52, 61, 73, 86, 101, 120]


# Array TT holds (p, m) combos
TT = []
for p in probs:
    for m in mags:
        TT.append((p, m))


# Init results array
results = []


# Take blockNum of TT permutations: those will be the generated tests in
# array shuffledBlocks

TTready = TT
shuffledBlocks = []

for i in range(Setup.blockNum):
    random.shuffle(TTready)
    block = TTready
    for statSet in block:
        # shuffledBlocks is the final array we draw trial conditions from
        shuffledBlocks.append(statSet)
