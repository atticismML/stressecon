import numpy as np
import Setup
from psychopy import sound

mrd = 2 # Modulation rate divisor
two_pi = 2*np.pi # (less complexity)

def wave(mag, prob, sfm, dur, freq):

    per = 1.0/freq
    time = np.arange(0, dur, per)
    halftime = np.arange(0, dur/2, per)

    if prob == 1: # If it's a sure bet or in the magnitude learning stage
        print 'mag: ' + str(mag)
        wave = np.sin(time*sfm*two_pi)
        print 'aye'
    elif mag == Setup.sureBet:
        wave = np.sin(time*sfm*two_pi) * np.sin(time*two_pi*(2**(mrd*prob)))
    else: # Else, add modulation tail
        wave1 = np.sin(halftime*sfm*two_pi)
        wave2 = np.sin(halftime*sfm*two_pi) \
            * np.sin(halftime*2*two_pi*(2**(mrd*prob)))
        wave = np.concatenate((wave1, wave2))

    return wave

def createToneValue(mag, prob, r, magB=Setup.sureBet, probB=1, min_freq=600, max_freq=2500, dur=2, freq=44100):

    # Lottery tone / ChoiceA in Learning Stage
    sinefreq = min_freq + (mag * (max_freq-min_freq)/Setup.magScale)
    waveA = wave(mag,prob,sinefreq,dur,freq)

    # Surebet tone / ChoiceB in Learning Stage
    sinefreq = min_freq + (magB * (max_freq-min_freq)/Setup.magScale)
    waveB = wave(magB, probB, sinefreq, dur, freq)

    # Depending on coinflip at runtine, set the tones in the proper channel

    if r == 0:
        waveLR = np.array([waveA, waveB]).transpose()

    elif r == 1:
        waveLR = np.array([waveB, waveA]).transpose()

    return waveLR


def soundMP(mag, prob, r):
    toneOut = sound.SoundPyo(value=createToneValue(mag, prob, r))
    return toneOut

