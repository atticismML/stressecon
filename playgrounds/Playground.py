#PLAYGROUND OF STRESS/ECON 

#import required libraries and edit preferences in-file
from psychopy import prefs
prefs.general['audioLib'] = ['pyo']
from psychopy import visual, core, sound, data, event, gui, iohub
from psychopy.tools.filetools import fromFile, toFile
import numpy as np
import random, math, pdb, csv


#create universe
mywin = visual.Window([900,600], monitor="testMonitor", units="deg")
tick = core.Clock() #for time tracking


#-------SETUP SUBJECT INFO, DATA FILES

#fetch data from preexisting params file, else create params .txt
try:
    expInfo = fromFile('current.txt')
except:
    expInfo = {'Subject':'Sylvain'}#default params, can be extended
expInfo['dateStr']= data.getDateStr() #date param

with open(file_path, 'Results') as outcsv:   
    #configure writer to write standard csv file
    writer = csv.writer(outcsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
    writer.writerow(['Choice Made', 'Sure Bet', 'Expected Util (M x P)', 'Magnitude' , 'Probability' , 'rSide' , 'rRow'])



#--------ADJUSTABLE VARIABLES

#SHADES OF COLOR

mid="#777777"
dark="#333333"
bright="#AAAAAA"
black="#000000"

#SCALE OF POKES
yscale = 3
xscale = 3
rightCol = 3
leftCol = -3

#DIMENSIONS
rows = 2
columns = 3
topRow = 3
bottomRow = 0

blockNum = 4

#SOUND

class ToneValue:

    def __init__(self,mag,prob,side,min_bl_freq=1000,max_bl_freq=8000, min_am_freq = 4, max_am_freq = 12,dur=1,freq=44100)
        
        per = 1.0/freq
        time = np.arange(0,dur,per)
        sinefreq = min_bl_freq + (mag * (max_bl_freq-min_bl_freq))
        amfreq = min_am_freq + (prob * (max_am_freq-min_am_freq))
        base_wave = np.sin(time*sinefreq)
        am_wave = np.sin(timeax*amfreq)+1)/2
        return(base_wave, am_wave)
    

    class Poke(visual.Polygon):
    """A class for the pokes, which inherits from visual.Polygon"""
    
    status='inactive' # By default, clicking on the poke means nothing,
    # but later on they may be lotteryChoice pokes, sureBet pokes, or ITI pokes
    
    def __init__(self,posx,posy,inColor=black,outColor=black,scale=1.5): 
        '''Initialize with position args, and optional 
        color args (for inset shape and outset bevel)'''
        
        # Each poke carries an outer bevel
        self.outer=visual.Polygon(win=mywin,edges=6,radius=scale,pos=(posx,posy))
        self.outer.fillColor=outColor 
        
        #But the poke itself shall be the inner shape:
        super(self.__class__,self).__init__(win=mywin,edges=6,radius=scale*0.7,pos=(posx,posy))
        self.fillColor=inColor
        
        # Set coordinates (centered)
        self.posx=posx
        self.posy=posy

    def draw(self):
        '''Draws shape and bevel. Be sure to update window after all drawing.'''
        self.outer.draw()
        super(self.__class__,self).draw()
        
    def getCoord(self):
        '''Returns the poke's x and y coordinates as a tuple'''
        x=self.posx
        y=self.posy
        return (x,y)

#--------------------------------        

#Some helpful functions

def drawPokes():
    '''Draw all the pokes in the 'pokes[]' array'''
    for p in pokes:
            p.draw() 
    mywin.update()
            
def place(scale,coeff,shift=0):
        """Know where to draw the pokes on an axis -- 
        this is based on assigned y-wise or x-wise scales, and a shift constant if needed"""

        return scale-(scale*coeff)+shift
            
def coordCheck(poke,x,y):
    '''Given a specific poke, check if it has these x and y coordinates'''
    if poke.getCoord()[0]==x and poke.getCoord()[1]==y:
        return True
    else:
        return False
    
def makeLivePoke(pokes,color,x,y,status):
    '''Set a specific poke to have a live status and a specific color'''
    for p in pokes:
        if coordCheck(p,x,y): 
            p.fillColor=color
            p.status=status

    
def makeChoicePokes(pokes,colorA,colorB,y):
    '''Set the pokes the user can click to make a decision--
    mainly dynamic based on the random y value generated at runtime'''
    makeLivePoke(pokes,colorA,rightCol,y,choiceR)
    makeLivePoke(pokes,colorB,leftCol,y,choiceL)
    
def resetPokes():
    '''Set all pokes inactive again'''
    for p in pokes: 
            p.fillColor=mid
            p.status='inactive'

            
#----MAIN            

if __name__ == "__main__": 

#Set the stats (risk combos)

    probs = [0.25, 0.5, 0.75]
    mags = [1, 2, 4, 8, 16, 32, 64]  
    TT = []

    #TT holds (p, m) combos
    for p in probs:
        for m in mags:
            TT.append((p,m))

    userChoices=[]

#Set the visuals
   
    pokes=[] #Get array ready -- We're going to make six Pokes and store them in a list
    pokeCoords = [] #Likewise, we need six coordinates
    for i in range(rows): # n physical rows times
        for j in range(columns): # n physical columns
            row=place(yscale,i) # Spacings based on scale
            col=place(xscale,j) 
            pokeCoords.append([col,row]) # Stores these coords in a list (for reference)
            pokes.append(Poke(posx=col,posy=row,inColor=mid)) # Generate a Poke object and store these in the list

#-------------------------GAMETIME
  
user = event.Mouse(True,None,None) #initiate mouse
curr_key = ''

shuffledBlocks = []

#Take blockNum of TT permutations: those will be the generated tests in array shuffledBlocks

for i in range(blockNum):
    block = random.shuffle(TT)
    shuffledBlocks.append(block)



trialNum = 0 

#....MAINLOOP

for trial in shuffledBlocks: # go through all blocks
 
# Flip coins for active poke placement
    rRow = random.randint(0,1)
    rSide = randon.randint(0,1)


if rRow == 1:
        makeLivePoke(pokes,black,0,topRow,'attention') #Top poke is the live one
    elif rRow == 0:
        makeLivePoke(pokes,black,0,bottomRow,'attention')# Bottom poke is the live one
        
    drawPokes()

    if rSide = 1:
        choiceR = 'lotteryPoke'
        choiceL = 'surePoke'
    elif rSide = 2:
        choiceR = 'surePoke'
        choiceL = 'lotteryPoke'
        

#-----------------------INTERTRIAL POKE TEST
    

    passed=False
   
    while 'q' not in curr_key and passed==False:
        curr_key = event.getKeys()
        if user.getPressed()!=[0,0,0]: 
            mpos = user.getPos()
            print mpos
            for poke in range(0,len(pokes)):
                if pokes[poke].contains(mpos):
                    if pokes[poke].status=='attention':
                        print 'Good!'
                        passed = True
                    user.clickReset()
                    resetPokes()
                       
#----------------------TRIAL: DECISION BETWEEN LOTTERY AND SURE BET
        
        
    probability = trial[0]
    magnitude = trial[1]
    #sureBet = 

    #Display stats 

    print probability
    print magnitude

    #print sureBet

    expectedUtil = magnitude*probability

    
    if r == 1:
        makeChoicePokes(pokes,dark,bright,topRow)
    elif r == 0:
        makeChoicePokes(pokes,dark,bright,bottomRow)
        
    drawPokes()
    
    curr_key = ''
    choiceMade = ''
    
    while 'q' not in curr_key and choiceMade=='':
        
        if user.getPressed()!=[0,0,0]: 
            mpos = user.getPos()
            print mpos
            for poke in range(0,len(pokes)):
                if pokes[poke].contains(mpos):
                    if pokes[poke].status=='lotteryPoke':
                        print "You chose the lottery"
                        choiceMade = 'lottery'
                    
                    if pokes[poke].status=='surePoke':
                        print "You chose the sure bet"
                        choiceMade = 'sure'
                    user.clickReset()

    result = [choiceMade, sureBet, expectedUtil, magnitude, probability, rSide, rRow]
    results.append(result)
    resetPokes()

    #------

    rRow = random.randint(0,1)
    rSide = randon.randint(0,1)

    if rRow == 1:
            makeLivePoke(pokes,black,0,topRow,'attention') #Top poke is the live one
    elif rRow == 0:
            makeLivePoke(pokes,black,0,bottomRow,'attention')# Bottom poke is the live one

    drawPokes()
    label = visual.TextStim(win, pos=[0,+3],text='Saving data')
    label.draw()
    mywin.update()

    with open(file_path, 'Results') as outcsv:   
        writer.writerow(result)


    #.....

    #At the end of this 'while', program returns to Main Loop 






                    
        
                    
                   


