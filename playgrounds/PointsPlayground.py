from psychopy import prefs
prefs.general['audioLib'] = ['pyo']
from psychopy import visual, core, sound, data, event, gui, iohub
from psychopy.tools.filetools import fromFile, toFile
import numpy as np
import random, math, pdb, csv


mywin = visual.Window([900,600], monitor="testMonitor", units="deg")

points = 754
hundreds = (points - (points % 100)) / 100
tens = (points%(hundreds*100)) / 10
ones = points - (hundreds*100) - (tens*10)


pileOrigin = []
pileOrigin = [-7,-5]
ypos = pileOrigin[1]

h_xpos = pileOrigin[0] + 1
t_xpos = h_xpos + 1
o_xpos = t_xpos + 1

def coin():
	return visual.Polygon(mywin, edges=32, radius=0.5,size=(1.0,0.5))

def stackCoins(denomination,stack_xpos,ypos,fill):
    for x in range(0,denomination):
        c = coin()
        cPos = pileOrigin 
        cPos[0] = stack_xpos
        cPos[1] = ypos
        ypos += 0.5
        print cPos
        c.setPos(cPos)
        c.fillColor = fill
        c.draw() 
    c_ypos = pileOrigin[1]

h_xpos = pileOrigin[0] + 1
t_xpos = h_xpos + 1
o_xpos = t_xpos + 1

stackCoins(hundreds,h_xpos,ypos, '#EEDD00')
print hundreds
stackCoins(tens,t_xpos,ypos, '#CCCCCC')
print tens
stackCoins(ones,o_xpos,ypos, '#EE9900')
print ones

mywin.update()
core.wait(5)

 