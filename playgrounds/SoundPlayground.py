#PLAYGROUND OF STRESS/ECON 

#import required libraries and edit preferences in-file
from psychopy import prefs
prefs.general['audioLib'] = ['pyo']
from psychopy import visual, core, sound, data, event, gui, iohub
from psychopy.tools.filetools import fromFile, toFile
import numpy as np
import random, math, pdb, csv
import time




def createToneValue(magL,magR,prob,min_freq=600,max_freq=6000,dur=2,freq=44100):
    
    
    per = 1.0/freq
    time = np.arange(0,dur,per)
    halftime = np.arange(0,dur/2,per)
    sinefreqL = min_freq + (magL * (max_freq-min_freq))
    sinefreqR = min_freq + (magR * (max_freq-min_freq))
    waveB2 = np.sin(time*sinefreqL*2*np.pi)*np.sin(time*2*3.1415*(2**(4*prob)))
    
   # waveB1 = np.sin(halftime*sinefreqL*2*np.pi)
   # waveB2 = np.sin(halftime*sinefreqL*2*np.pi)*np.sin(halftime*2*3.1415*(2**(4*prob)))
    waveS = np.sin(time*sinefreqR*2*np.pi)/4
    
    waveB = np.concatenate((waveB1,waveB2))
    waveLR = np.array([waveB,waveS]).transpose()

    return waveLR
    
    
def soundLR(magL, magR,prob):
    toneOut = sound.SoundPyo(value=createToneValue(magL,magR,prob))
    return toneOut

A = soundLR(0.8,0,.25)

B = soundLR(0.5,0,.5)
C = soundLR(0.6,0,.75)

mywin = visual.Window([900,600], monitor="testMonitor", units="deg")
delvar=0.05

shape = visual.Polygon(win=mywin,edges=6,radius=3)
shape.fillColor="#FFFFFF"
shape.draw()
mywin.update()
core.wait(delvar)

A.play()
core.wait(2.1)

shape.fillColor="#0000FF"
shape.draw()
mywin.update()
core.wait(delvar)
B.play()
core.wait(2.1)

shape.fillColor="#FF0000"
shape.draw()
mywin.update()
core.wait(delvar)

C.play()
core.wait(2.1)

shape.fillColor="#00FF00"
shape.draw()
mywin.update()
core.wait(delvar)

B.play()
